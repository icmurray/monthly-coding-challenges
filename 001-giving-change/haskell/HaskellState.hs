import Control.Monad.State.Lazy
import Data.Maybe
import Data.Traversable
import Data.Map (Map)
import qualified Data.Map as Map

newtype Coin = Coin Int deriving Show

-- store computed solutions: (Coins idx, Target) -> Soln
type Memo = Map (Int, Int) Soln

-- interim soution: distribution of coins, total coins used
data Soln = Soln [Int] Int deriving Show

-- wrapper that zips up a distribution with the orignal denominations
data Change = Change [(Coin, Int)] deriving Show

euros :: [Coin]
euros = map Coin [200, 100, 50, 20, 10, 5, 2, 1]

preDecimal :: [Coin]
preDecimal = map Coin [60, 30, 24, 12, 6, 3, 1]

solve :: [Coin] -> Int -> Maybe Change
solve cs = fst . solveInspect cs

-- returns the Memo state that was computed too
solveInspect :: [Coin] -> Int -> (Maybe Change, Memo)
solveInspect cs target = (fmap (mkChange cs) soln, memo)
  where (soln, memo) = runState (change cs target) (Map.empty)

mkChange :: [Coin] -> Soln -> Change
mkChange cs soln = Change $ zip cs (distribution soln)

change :: [Coin] -> Int -> State Memo (Maybe Soln)
change _  0 = return $ Just emptySoln
change [] _ = return Nothing
change (c:cs) target = memoize (c:cs) target step
  where step         = fmap (chooseMin . dropEmpty) candidates
        candidates   = sequence $ [fmap (extend n) (subProblem n) | n <- coinAmounts]
        coinAmounts  = [0,1 .. target `div` (coinValue c)]
        subProblem n = change cs (target - n * (coinValue c))

coinValue :: Coin -> Int
coinValue (Coin c) = c

coinCount :: Soln -> Int
coinCount (Soln _ count) = count

distribution :: Soln -> [Int]
distribution (Soln d _) = d

extend :: Int -> Maybe Soln -> Maybe Soln
extend = fmap . extendSoln

extendSoln :: Int -> Soln -> Soln
extendSoln n (Soln cs n0) = Soln (n:cs) (n0 + n)

dropEmpty :: [Maybe a] -> [a]
dropEmpty = catMaybes

-- I'm sure there's a better way of defining this using existing functions,
-- but I'm not sure what it is.
chooseMin :: [Soln] -> Maybe Soln
chooseMin []         = Nothing
chooseMin [s]        = Just s
chooseMin (s1:s2:ss) = if coinCount s1 <= coinCount s2 then chooseMin (s1:ss) else chooseMin (s2:ss)

emptySoln :: Soln
emptySoln = Soln [] 0

-- if a Soln already exists, retrieve it from the memoized results
-- otherwise compute it using `calc`, and store the result
memoize :: [Coin] -> Int -> State Memo (Maybe Soln) -> State Memo (Maybe Soln)
memoize coins target calc = do
  memo <- get
  let res = Map.lookup (length coins, target) memo
  case res of
    Nothing -> do
      inner <- calc
      case inner of
        Nothing -> return Nothing
        Just s -> do
          _ <- modify (Map.insert (length coins, target) s)
          return $ Just s
    Just s -> return $ Just s
