val scala3Version = "3.2.1"

lazy val root = project
  .in(file("."))
  .settings(
    name := "giving change",
    version := "0.1.0-SNAPSHOT",

    scalaVersion := scala3Version,

    libraryDependencies += "org.typelevel" %% "cats-core" % "2.8.0",
    libraryDependencies += "org.scalameta" %% "munit" % "0.7.29" % Test,

    Compile / run / fork := true
  )
