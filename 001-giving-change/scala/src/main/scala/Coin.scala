
final case class Coin(value: Int)

object Coin {

  implicit val ord: Ordering[Coin] = Ordering.by(_.value)

}
