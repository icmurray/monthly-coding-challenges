
trait MemoTable {
  def memoize(coins: Seq[Coin], target: Int)(op: => Option[Soln]): Option[Soln]
}

/**
 * Tried different implementations of MemoTable.
 *
 * LongMap worked better than a generic HashMap as avoided hash-computations
 * and probably some collisions too.
 *
 * An Array-based implementation worked well until it became very large, and
 * required a large allocation from the heap.  At which point, a sparser
 * representation worked better.
 **/
object MemoTable {

  def byLongMap(maxTarget: Int): MemoTable = new MemoTable {

    private val mem = new scala.collection.mutable.LongMap[Soln]()
    private def key(coins: Int, target: Int): Long = (coins * (maxTarget + 1)) + target

    override def memoize(coins: Seq[Coin], target: Int)(op: => Option[Soln]): Option[Soln] = {
      val k = key(coins.length, target)
      mem.get(k) match {
        case None =>
          val soln = op
          soln.foreach(mem.update(k, _))
          soln
        case soln @ Some(_) =>
          soln
      }
    }
  }

}
