import scala.collection.mutable.{ Map => MMap }

import Change._
import Coin._

/**
 * Uses memoization to avoid recomputing each sub-problem
 * more than once.
 *
 * Uses a mutable Map to share memoized calculations
 *
 * Uses an analysis of the coins to determine the maximum number of
 * times any particular coin should be used.  This can reduce the
 * search space for sets of coins where they are multiple of one another.
 * It doesn't help much with some sets of coins, eg. [1,3,7,11,23,31,59]
 **/
object MemoizedWithAnalysis extends Change {

  override def solve(coins: Seq[Coin], target: Int): Option[ChangeGiven] = {
    assert (target >= 0)
    val memo = MemoTable.byLongMap(target)
    val ordered = coins.sorted.reverse
    val analysis = CoinAnalysis.analyse(coins)
    change(ordered.toList, target, memo, analysis).map { soln =>
      ChangeGiven(ordered.zip(soln.dist))
    }

  }

  private def change(coins: List[Coin], target: Int, memo: MemoTable, analysis: CoinAnalysis): Option[Soln] =

    (target, coins) match {

      case (0, _) =>
        Some(Soln.empty)

      case (_, Nil) =>
        None

      case (target, c :: cs) =>
        memo.memoize(coins, target) {
          val candidates = for {
            n <- (0 to analysis.limitFor(c, target))
            sub <- change(cs, target - n * c.value, memo, analysis)
            candidate = sub.extendWith(c, n)
          } yield candidate

          candidates.minByOption(_.coinCount)
        }
    }
}
