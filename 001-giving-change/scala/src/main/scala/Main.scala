import Change._

object Main {

  def main(args: Array[String]): Unit = {

    val solvers = Seq(
      // SimpleRecursive,
      // Memoized,
      // MemoizedWithAnalysis,
      StateMonad
    )

    val euros = List(1,2,5,10,20,50,100,200).map(Coin.apply)
    val predecimals = List(1,3,6,12,24,30,60).map(Coin.apply)
    val primes = List(1,3,7,11,23,31,59).map(Coin.apply)
    val ones = List(1).map(Coin.apply)

    val problems = Seq(
      ones -> 3,
      // euros -> 7,
      // euros -> 191,
      // euros -> 191039,
      predecimals -> 48,

      predecimals -> 51,
      predecimals -> 96,
      // predecimals -> 132,
      // predecimals -> 411,
      predecimals -> 513,
      // predecimals -> 5138,
      // predecimals -> 51382,
      // predecimals -> 71382,
      // predecimals -> 513823,
      // predecimals -> 5138235,
      // predecimals -> 51382352,
      // predecimals -> 513823529,
    )

    val results = for {
      (coins, target) <- problems
      solver <- solvers
    } yield verifySolution(coins, target, solver)

  }

  private def verifySolution(coins: Seq[Coin], target: Int, solver: Change) =
    solver.solve(coins, target) match {
      case None =>
        printNoSolution(coins, target)
      case Some(soln) if soln.changeValue == target =>
        printSuccess(soln)
      case Some(soln) =>
        printFailure(soln, target)
    }

  private def printNoSolution(coins: Seq[Coin], target: Int): Unit = {
    val line = s"[- coins]\t${target}"
    println(Console.YELLOW + line + Console.RESET)
  }

  private def printSuccess(solution: ChangeGiven): Unit = {
    val line = s"[${solution.coinCount} coins]\t${solution.changeValue} \t==  ${distString(solution.distribution)}"
    println(Console.GREEN + line + Console.RESET)
  }

  private def printFailure(solution: ChangeGiven, target: Int): Unit = {
    val line = s"[${solution.coinCount} coins]\t${target} \t!=  ${distString(solution.distribution)} (${solution.changeValue})"
    println(Console.RED + line + Console.RESET)
  }

  private def distString(dist: Seq[(Coin, Int)]): String =
    dist
      .filter(_._2 != 0)
      .sortBy(_._1)
      .reverse
      .map { case (c, n) => s"${c.value}*$n" }
      .mkString(" + ")

}
