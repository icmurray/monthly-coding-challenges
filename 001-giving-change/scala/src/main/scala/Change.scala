

object Change {

  final case class ChangeGiven(distribution: Seq[(Coin, Int)]) {
    val coinCount = distribution.unzip._2.sum
    val changeValue = distribution.map { (coin, n) => coin.value * n }.sum
  }

}

trait Change {

  import Change._

  def solve(coins: Seq[Coin], target: Int): Option[ChangeGiven]

}
