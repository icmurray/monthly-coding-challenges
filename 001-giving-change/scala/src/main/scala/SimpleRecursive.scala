import Change._
import Coin._

/**
 * Exponential in the size of the target.
 **/
object SimpleRecursive extends Change {

  override def solve(coins: Seq[Coin], target: Int): Option[ChangeGiven] = {
    assert (target < 0)
    val ordered = coins.sorted.reverse
    change(ordered.toList, target).map { soln =>
      ChangeGiven(ordered.zip(soln.dist))
    }
  }

  private def change(coins: List[Coin], target: Int): Option[Soln] =

    (target, coins) match {

      case (0, _) =>
        Some(Soln.empty)

      case (_, Nil) =>
        None

      case (target, c :: cs) =>
        val candidates = for {
          n <- (0 to target / c.value)
          sub <- change(cs, target - n * c.value)
          candidate = sub.extendWith(c, n)
        } yield candidate

        candidates.minByOption(_.coinCount)
    }

}
