/**
 * Given a set of coins, eg [1d ,3d ,12d ,24d ,30d ,60d], then we can tell just by looking
 * at them that there's no point ever using 3 x 1d because 1 x 3d provides the same value
 * with strictly fewer coins.  This applies to other combinations too, eg. 4 x 3d == 1 x 12d.
 *
 * This doesn't just apply to pairs of coins where the larger is a multiple of the smaller.
 * For example, with 24d and 30d: 5 x 24d == 120d == 4 x 30d.  So there's no point in ever
 * choosing 5 x 24d because it's always better to choose 4 x 30d.
 *
 * This means that for every coin (except the largest), we can set a limit on the number of
 * times that coin should ever be picked.  For the list above:
 *
 *  - 1d  --> 2 x
 *  - 3d  --> 3 x
 *  - 12d --> 1 x
 *  - 24d --> 4 x
 *  - 40d --> 1 x
 *  - 60d --> ∞ x
 **/
case class CoinAnalysis(m: Map[Coin, Int]) {

  def limitFor(c: Coin, target: Int) =
    Math.min(
      target / c.value,
      m.get(c).getOrElse(Integer.MAX_VALUE)
    )
}

object CoinAnalysis {

  def analyse(coins: Seq[Coin]): CoinAnalysis =
    CoinAnalysis(
      coins
        .sorted
        .combinations(2).toSeq
        .collect { case Seq(c1: Coin, c2: Coin) => (c1, c2) }
        .map { case (c1, c2) => (c1, (leastCommonMultiple(c1.value ,c2.value) / c1.value) - 1) }
        .groupMap(_._1)(_._2)
        .mapValues(_.min)
        .toMap
    )

  private def leastCommonMultiple(n: Int, m: Int): Int = {
    import math.BigInt.int2bigInt
    assert(n > 0)
    assert(m > 0)
    val gcd = n.gcd(m).toInt
    n * m / gcd
  }
}
