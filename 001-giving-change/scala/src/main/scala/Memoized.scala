import scala.collection.mutable.{ Map => MMap }

import Change._
import Coin._

/**
 * Uses memoization to avoid recomputing each sub-problem more than once.
 *
 * Uses a mutable Map to share memoized calculations
 *
 * Still not great performance-wise.  Found a memoized recursive solution based
 * on recursing on only the target performed better.
 **/
object Memoized extends Change {

  override def solve(coins: Seq[Coin], target: Int): Option[ChangeGiven] = {
    assert (target >= 0)
    val memo = MemoTable.byLongMap(target)
    val ordered = coins.sorted.reverse
    change(ordered.toList, target, memo).map { soln =>
      ChangeGiven(ordered.zip(soln.dist))
    }
  }

  private def change(coins: List[Coin], target: Int, memo: MemoTable): Option[Soln] =

    (target, coins) match {

      case (0, _) =>
        Some(Soln.empty)

      case (_, Nil) =>
        None

      case (target, c :: cs) =>
        memo.memoize(coins, target) {
          val candidates = for {
            n <- (0 to target / c.value)
            sub <- change(cs, target - n * c.value, memo)
            candidate = sub.extendWith(c, n)
          } yield candidate

          candidates.minByOption(_.coinCount)
        }
    }
}
