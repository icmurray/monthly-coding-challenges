import scala.collection.immutable.LongMap
import scala.collection.mutable.{ Map => MMap }

import cats.syntax.all._
import cats.data.State

import Change._
import Coin._
import cats.data.IndexedStateT

/**
 * Uses memoization to avoid recomputing each sub-problem more than once.
 *
 * Uses a mutable Map to share memoized calculations
 *
 * Still not great performance-wise.  Found a memoized recursive solution based
 * on recursing on only the target performed better.
 **/
object StateMonad extends Change {

  override def solve(coins: Seq[Coin], target: Int): Option[ChangeGiven] = {
    assert (target >= 0)
    val memo = Memo.init(target)
    val ordered = coins.sorted.reverse
    change(ordered.toList, target).map { res =>
      res.map { soln =>
        ChangeGiven(ordered.zip(soln.dist))
      }
    }.runA(Memo.init(target)).value
  }

  private def change(coins: List[Coin], target: Int): State[Memo, Option[Soln]] =

    (target, coins) match {

      case (0, _) =>
        State.pure(Some(Soln.empty))

      case (_, Nil) =>
        State.pure(None)

      case (target, c :: cs) =>
        Memo.memoize(coins, target) {

          val candidates: Seq[State[Memo, Seq[Soln]]] = (0 to target / c.value).map { n =>
            change(cs, target - n * c.value).map { sub =>
              sub.toSeq.map(_.extendWith(c, n))
            }
          }

          candidates
            .flatSequence
            .map(_.minByOption(_.coinCount))
        }
    }


  private object Memo {

    def init(maxTarget: Int): Memo = Memo(maxTarget, LongMap.empty)

    def memoize(coins: Seq[Coin], target: Int)(calc: => State[Memo, Option[Soln]]): State[Memo, Option[Soln]] =
      State.get[Memo].flatMap { memo =>
        val k = memo.key(coins, target)
        memo.lookup.get(k) match {
          case None =>
            calc.flatMap {
              case None =>
                State.pure(None)
              case Some(soln) =>
                State.modify[Memo](memo => memo.copy(lookup = memo.lookup + (k -> soln))) as Some(soln)
            }
          case soln @ Some(_) =>
            State.pure(soln)
        }
      }
  }

  private case class Memo(maxTarget: Int, lookup: LongMap[Soln]) {

    private def key(coins: Seq[Coin], target: Int): Long =
      (coins.length * (maxTarget + 1)) + target

  }
}
