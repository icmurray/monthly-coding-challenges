
/**
 * Helps to avoid recomputing coinCount
 **/
private case class Soln(dist: List[Int], coinCount: Int) {
  def extendWith(c: Coin, n: Int): Soln =
    Soln(n :: dist, n + coinCount)
}

private object Soln {
  val empty = Soln(Nil, 0)
}
