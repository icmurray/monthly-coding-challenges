{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Data.Aeson (ToJSON(..), FromJSON(..), decode', encode)
import qualified Data.ByteString.Lazy.Char8 as B
import           Data.Maybe (fromJust)
import           GHC.Generics
import           Text.RawString.QQ

data Item = Item
  { name :: String
  , value :: Int
  , weight :: Int
  } deriving (Show, Generic, ToJSON, FromJSON)

data Result = Result
  { totalWeight :: Int
  , totalValue :: Int
  , items :: [Item]
  } deriving (Show, Generic, ToJSON)

main :: IO ()
main = B.putStrLn (encode result)
    where result = solve 100 problem

solve :: Int -> [Item] -> Result
solve _ [] = emptyResult
solve capacity (i:is) =
      if spaceForItem && (totalValue withItem > totalValue withoutItem)
          then withItem
          else withoutItem
      where spaceForItem = weight i <= capacity
            withItem     = extend (solve (capacity - (weight i)) is) i
            withoutItem  = solve capacity is

emptyResult :: Result
emptyResult = Result 0 0 []

extend :: Result -> Item -> Result
extend r i = Result newWeight newValue newItems
    where newWeight = (totalWeight r) + (weight i)
          newValue  = (totalValue r) + (value i)
          newItems  = i : (items r)

example_1 :: [Item]
example_1 = fromJust $ decode' json
  where json = [r|
[
  {"name": "Cycling shorts", "value": 20, "weight": 30},
  {"name": "Running shoes", "value": 18, "weight": 30},
  {"name": "Gym clothes", "value": 12, "weight": 19},
  {"name": "Evening wear", "value": 22, "weight": 40},
  {"name": "Chargers", "value": 1, "weight": 12}
]
|]

problem :: [Item]
problem = fromJust $ decode' json
  where json = [r|
[
  {"name": "Cycling shorts", "value": 20, "weight": 30},
  {"name": "Running shoes", "value": 18, "weight": 30},
  {"name": "Gym clothes", "value": 12, "weight": 19},
  {"name": "Evening wear", "value": 22, "weight": 40},
  {"name": "Chargers", "value": 1, "weight": 12},
  {"name": "Toothbrush", "value": 7, "weight": 4},
  {"name": "Book", "value": 16, "weight": 22},
  {"name": "Sunscreen", "value": 8, "weight": 14},
  {"name": "Coat", "value": 21, "weight": 36},
  {"name": "Hat", "value": 6, "weight": 9},
  {"name": "Hairbrush", "value": 17, "weight": 26},
  {"name": "Sunglasses", "value": 11, "weight": 18},
  {"name": "Flip flops", "value": 23, "weight": 40},
  {"name": "Ear plugs", "value": 5, "weight": 13},
  {"name": "Emergency contact information", "value": 14, "weight": 31},
  {"name": "Travel insurance documents", "value": 3, "weight": 7},
  {"name": "Yoga mat", "value": 10, "weight": 16},
  {"name": "Camera", "value": 13, "weight": 32},
  {"name": "Power bank", "value": 7, "weight": 28},
  {"name": "Portable WiFi hotspot", "value": 15, "weight": 52},
  {"name": "Headphones", "value": 10, "weight": 36},
  {"name": "E-reader", "value": 1, "weight": 6},
  {"name": "Insect repellent", "value": 16, "weight": 59},
  {"name": "First-aid kit", "value": 5, "weight": 12},
  {"name": "Toothpaste", "value": 10, "weight": 39},
  {"name": "Dental floss", "value": 2, "weight": 7},
  {"name": "Mouthwash", "value": 1, "weight": 7},
  {"name": "Deodorant", "value": 14, "weight": 47},
  {"name": "Razor", "value": 6, "weight": 18},
  {"name": "Hand sanitizer", "value": 16, "weight": 32},
  {"name": "Travel blanket", "value": 8, "weight": 18},
  {"name": "Water bottle", "value": 14, "weight": 31},
  {"name": "Reusable shopping bag", "value": 4, "weight": 12},
  {"name": "Laptop", "value": 19, "weight": 24},
  {"name": "Snacks", "value": 9, "weight": 31},
  {"name": "Waterproof phone case", "value": 13, "weight": 48},
  {"name": "Outlet adapter", "value": 9, "weight": 8},
  {"name": "Swimsuit", "value": 5, "weight": 17},
  {"name": "Beach towel", "value": 10, "weight": 36},
  {"name": "Hiking boots", "value": 13, "weight": 43},
  {"name": "Hiking socks", "value": 4, "weight": 14},
  {"name": "Umbrella", "value": 5, "weight": 19},
  {"name": "Gloves", "value": 5, "weight": 13},
  {"name": "Scarf", "value": 1, "weight": 6},
  {"name": "Swedish phrase book", "value": 8, "weight": 8},
  {"name": "Arm bands", "value": 13, "weight": 12},
  {"name": "Sleepwear", "value": 7, "weight": 27},
  {"name": "Contact lenses", "value": 10, "weight": 34},
  {"name": "Deck of cards", "value": 15, "weight": 40},
  {"name": "Travel board games", "value": 12, "weight": 36},
  {"name": "Luggage tags", "value": 5, "weight": 10},
  {"name": "Journal", "value": 13, "weight": 38},
  {"name": "Pencil", "value": 12, "weight": 36},
  {"name": "Neck pillow", "value": 14, "weight": 18},
  {"name": "Compass", "value": 6, "weight": 16},
  {"name": "Sleeping bag", "value": 13, "weight": 34},
  {"name": "Documents", "value": 15, "weight": 10},
  {"name": "Tent", "value": 1, "weight": 7},
  {"name": "Camp stove", "value": 1, "weight": 6},
  {"name": "Hiking poles", "value": 11, "weight": 37},
  {"name": "Fishing gear", "value": 13, "weight": 41},
  {"name": "Bike helmet", "value": 10, "weight": 33},
  {"name": "Surfboard", "value": 4, "weight": 16},
  {"name": "Sandals", "value": 9, "weight": 31},
  {"name": "Watch", "value": 10, "weight": 21},
  {"name": "Knitting", "value": 1, "weight": 6},
  {"name": "Travel iron", "value": 10, "weight": 34}
]
|]
