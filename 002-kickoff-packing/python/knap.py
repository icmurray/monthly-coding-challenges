#!/usr/bin/env python3

import functools
import json

def main():
    result = solve(100, problem())
    print(result)

def solve(capacity, items):

    """
    The cache prevents O(2^n) runtime by reusing solutions
    to the sub-problems
    """
    @functools.cache
    def _solve(capacity, item_index):

        # no more items to consider
        if item_index == len(items):
            return _empty_soln()

        # consider whether to include this item in the solution or not
        item = items[item_index]

        # find optimal solution without the current item
        without_item = _solve(capacity, item_index + 1)

        # if there's capacity, also check the optimal solution with
        # the current item
        if (item['weight'] <= capacity):
            with_item = _solve(capacity - item['weight'], item_index + 1)

            # ... and pick the best
            if with_item['total_value'] + item['value'] > without_item['total_value'] :
                return _extend_soln(with_item, item)
            else:
                return without_item
        else:
            return without_item

    return _solve(capacity, 0)

def _empty_soln():
    return {
        'total_value': 0,
        'total_weight': 0,
        'items': []
    }

"""
Return a new solution by extending the given one with the given item
"""
def _extend_soln(soln, item):
    return {
        'total_value': soln['total_value'] + item['value'],
        'total_weight': soln['total_weight'] + item['weight'],
        'items': [item] + soln['items']
    }

def problem():
    with open("input.json") as f_in:
        return json.load(f_in)

if __name__ == '__main__':
    main()
